package com.lato.dao;

import com.lato.entity.Customer;

import java.util.List;

public interface CustomerDAO {

    public List<Customer> getCustomers();

}
